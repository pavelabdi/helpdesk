import React from 'react'
import ReactDOM from 'react-dom'
import {
    BrowserRouter,
    Route
  } from 'react-router-dom'
import Login from './components/Login'
import Tickets from './components/Tickets'
import CreateTicket from './components/CreateTicket'
import TicketOverview from './components/TicketOverview'
import TicketEdit from './components/TicketEdit'
import TicketFeedback from './components/TicketFeedback'

ReactDOM.render(
    <BrowserRouter>
        <Route path="/login" component={Login}></Route>
        <Route exact path="/tickets" component={Tickets}></Route>
        <Route exact path="/tickets/:id" component={TicketOverview}></Route>
        <Route exact path="/tickets/:id/edit" component={TicketEdit}></Route>
        <Route exact path="/tickets/:id/feedback" component={TicketFeedback}></Route>
        <Route exact path="/create" component={CreateTicket}></Route>
    </BrowserRouter>, 
document.getElementById('root'));