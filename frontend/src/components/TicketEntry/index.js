import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
const uuidv1 = require('uuid/v1')

class TicketEntry extends Component {

    state = {
        selectedbutton: '',
        buttonVisible: false
    }

    componentDidMount() {
        console.log(this.props.arr)      
        this.setState({
            selectedbutton: this.props.type === 'ticket' && this.props.arr.actions.length > 0 ? this.props.arr.actions[0] : '',
            buttonVisible: this.props.type === 'ticket' && this.props.arr.actions.length > 0
        })
    }

    componentWillReceiveProps(nextProps){
        this.nextProps.onUpdate()
        this.setState({
            selectedbutton: this.props.type === 'ticket' && nextProps.arr.actions.length > 0 ? nextProps.arr.actions[0] : '',
            buttonVisible: this.props.type === 'ticket' && nextProps.arr.actions.length > 0
        })  
    }

    selectButton = (text) => {
        this.setState({ selectedbutton: text });
    }

    renderButton = () => {
        let optionItems = this.props.arr.actions.map((action) =>
            <li key={uuidv1()} className="dropdown-item" onClick={() => this.selectButton(`${action}`)}>{action}</li>
        );
        return (
            <>
                <div className="btn-group">
                    <button className="btn btn-primary" onClick={() => this.props.onClick(this.props.arr.id, this.state.selectedbutton)}>{this.state.selectedbutton}</button>
                    <button id="btnGroupDrop1" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        <span className="caret"></span>
                    </button>
                    <div className="dropdown-menu">
                        {optionItems}
                    </div>
                </div>
            </>
        )
    }

    render() {
        let link = `/tickets/${this.props.arr.id}`
        console.log(this.props)
        return (      
            <tr>
            {this.props.type === 'ticket' ? 
                (<>
                <td>{this.props.arr.id}</td>
                <td><a href={link}>{this.props.arr.name}</a></td>
                <td>{this.props.arr.dueDate}</td>
                <td>{this.props.arr.urgency}</td>
                <td>{this.props.arr.state}</td>
                <td>{this.state.buttonVisible ? this.renderButton() : null}</td>
                </>) : this.props.type === 'history' ?
                (<>
                <td>{this.props.arr.date}</td>
                <td>{this.props.arr.user}</td>
                <td>{this.props.arr.action}</td>
                <td>{this.props.arr.description}</td>
                </>) : 
                (<>
                <td>{this.props.arr.date}</td>
                <td>{this.props.arr.user}</td>
                <td>{this.props.arr.text}</td>
                </>)
            }
            </tr> 
        )
    }
}

export default TicketEntry;
