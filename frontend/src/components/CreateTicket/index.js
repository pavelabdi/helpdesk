import React from "react";
import { Redirect } from 'react-router-dom';
import File from '../File'
import 'bootstrap/dist/css/bootstrap.css'
import './style.css'

const uuidv1 = require('uuid/v1')
const token = localStorage.token

class CreateTicket extends React.Component {
    state = {
        items: [],
        name: '',
        description: '',
        category: 'Application & Services',
        urgency: 'Critical',
        dueDate: '',
        comment: '',
        file: null,
        attachment: null,
        uploadedfiles: [],
        myFiles: {},
        selectedfile: '',
        redirectToAllTickets: false,
        redirectToCreatedTicket: false,
        redirectId: '',
        error: null
    }

    renderMyFiles = () => {
        return (
          Object.keys(this.state.selectedfile).map((el) => {
            return (
              <File onClick={this.handleDelClick} keyid={el} key={uuidv1()} text={this.state.selectedfile[el].name} />          
            )
          })
        )
      }

    handleDelClick = (id) => {
        this.setState(
            (prevState) => {
                let newObj = {...prevState.myFiles}
                delete newObj[id]
                return {
                    myFiles: newObj
                }
            }
        )
        console.log(this.state.myFiles)
    }

    onFormSubmit = (event) => {
        event.preventDefault() // Stop form submit
        this.fileUpload(this.state.file).then((response) => {
            console.log(response.data);
        })
    }

    onChange = (event) => {
        let target = event.currentTarget
        this.setState((prevState) => { 
            console.log(prevState)
            let  newObj = {...prevState.myFiles}
              newObj[`${Date.now()}`] = prevState.selectedfile
              return {
                [target.name]: target.value,
                file: target.files[0],
                myFiles: newObj,
                selectedfile: target.files
              } 
        })
        // this.setState({ 
        //         file: event.target.files[0]
        // })
        //console.log(this.state)
    }

    fileUpload = (file) => {
        const url = 'http://localhost:8080/attachments';
        const formData = new FormData();
        formData.append('file', file)
        return fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: formData
            }
        )       
    }

    componentDidMount() {
        let categories = [];
        fetch('http://localhost:8080/categories', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                }
            })
            .then(response => response.json())
            .then(data => {
                categories = data.map((item) => {
                    return item
                });                                               
                this.setState({
                    items: categories
                });
            });
    }

    handleChange = (event) => {
        event.preventDefault()
        this.setState({ [event.target.name]: event.target.value })
    }

    renderRedirectReturn = () => {
        if (this.state.redirectToAllTickets) {
            return <Redirect to='/tickets' />
        }
    }

    renderRedirectCreate = () => {
        //console.log(this.state.redirectId)
        if (this.state.redirectToCreatedTicket) {
            return <Redirect to={`/tickets/${this.state.redirectId}`} />
        }
    }

    render() {
        let items = this.state.items;
        let optionItems = items.map((item) =>
            <option key={item}>{item}</option>
        );
        return (
            <>
                {this.renderRedirectReturn()}
                {this.renderRedirectCreate()}
                <button className="btn btn-success" onClick={this.redirectToTickets}>Ticket list</button>
                <h2>Create new Ticket</h2>
                <div className='create-ticket__container'>
                    <label style={{width: '80%'}}>Category:
                        <select className="form-control" name="category" onChange={this.handleChange} value={this.state.category}>
                            {optionItems}
                        </select>
                    </label>
                    <label style={{width: '80%'}}>Name:
                        <input type="text" className="form-control" name="name" value={this.state.name} onChange={this.handleChange} />
                    </label>
                    <label>{this.state.error == null ? null : this.state.error}</label>
                    <label style={{width: '80%'}}>Description:
                        <textarea type="text" className="form-control" name="description" value={this.state.description} onChange={this.handleChange} />
                    </label>
                    <label style={{width: '80%'}} onChange={this.handleChange} value={this.state.urgency}>Urgency:
                        <select className="form-control" name="urgency">
                            <option>Critical</option>
                            <option>High</option>
                            <option>Average</option>
                            <option>Low</option>
                        </select>
                    </label>
                    <label style={{width: '80%'}}>Desired Resolution Date:
                        <input type="date" className="form-control" name="dueDate" value={this.state.dueDate} onChange={this.handleChange} />
                    </label>
                    <label style={{width: '80%'}}>Add attachments
                    <form onSubmit={this.onFormSubmit} >
                        <input type="file" onChange={this.onChange} name="uploadedfiles[]" multiple/>
                        <button type="submit" className="btn btn-light">Upload</button>
                    </form>
                    <label>Files{this.renderMyFiles()}</label>
                    </label>
                    <label style={{width: '80%'}}>Comment:
                        <textarea type="text" className="form-control" name="comment" value={this.state.comment} onChange={this.handleChange} />
                    </label>
                </div>
                <div className="btn-group float-right">
                    <button className="btn btn-light" onClick={this.handleSave}>Save as Draft</button>
                    <button className="btn btn-success" onClick={this.handleSubmit}>Submit</button>
                </div>
            </>
        );
    }

    handleClick = () => {
        const { params } = this.props.match
        let request = ''
        if (params.id === undefined){
            request = 'http://localhost:8080/attachments/1'
        }else {
            request = `http://localhost:8080/attachments/${params.id}`
        }
        this.setState({
            attachment : request
        })        
    }

    handleSubmit = () => {
        let action = 'create'
        fetch(`http://localhost:8080/tickets/${action}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                category: this.state.category,
                name: this.state.name,
                description: this.state.description,
                urgency: this.state.urgency,
                dueDate: this.state.dueDate,
                comment: this.state.comment
            })
        })
        .then((response) => {
            if (response.status !== 200) {
                response.json()
                .then((responseJson) => {
                    this.setState({
                        error: responseJson.message
                    })
                })
                console.log(response)
                return;
            }
            response.json()            
            .then((responseJson) => {
                this.setState({
                    redirectId: responseJson.ticketId,
                    redirectToCreatedTicket: true,
                    error: null
                })
            })
        });
    }

    handleSave = () => {
        let action = 'save_as_draft'
        fetch(`http://localhost:8080/tickets/${action}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                category: this.state.category,
                name: this.state.name,
                description: this.state.description,
                urgency: this.state.urgency,
                dueDate: this.state.dueDate,
                comment: this.state.comment
            })
        }).then(() => {
            this.redirectToTickets()
        });
    }

    redirectToTickets= () => {
        this.setState({
            redirectToAllTickets: true
        });
    }
}

export default CreateTicket;