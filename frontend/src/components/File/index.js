import React from 'react';
import './style.css';

class File extends React.Component {
  
  render() {
    return (
      <div className='file__container'>
        <li>
            {this.props.text}
        </li>
        <button onClick={() => {this.props.onClick(this.props.keyid)}}>delete</button>
      </div>
    )
  }
}

export default File