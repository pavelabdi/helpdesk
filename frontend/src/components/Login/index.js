import React, { Component } from 'react'
import AuthService from '../../service/AuthService';
import 'bootstrap/dist/css/bootstrap.css'
import './style.css'

class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      message: '',
    }
  }

  componentDidMount() {
    localStorage.clear();
  }

  handleChange = (event) => {
    event.preventDefault();
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const credentials = { username: this.state.username, password: this.state.password }
    AuthService.login(credentials).then(res => {
      console.log(res)
      if (res.status === 200) {
        localStorage.setItem("token", res.data.token)
        this.props.history.push('/tickets')
        this.setState({ message: "" })
      } else {
        this.setState({ message: "Authorization failed" })
        this.props.history.push('/login')
      }
    })
  }

  render() {
    return (
      <>
        <h2>Login to the Help Desk</h2>
        <div className='login__container'>
          <label>{this.state.message}</label>
          <label style={{ width: '80%' }}>User Name:
            <input type="text" className="form-control" name="username" value={this.state.username} onChange={this.handleChange} />
          </label>
          <label style={{ width: '80%' }}>Password:
            <input type="text" className="form-control" name="password" value={this.state.password} onChange={this.handleChange} />
          </label>
        </div>
        <div className="btn-group float-right">
          <button className="btn btn-light" onClick={this.handleSubmit}>Enter</button>
        </div>
      </>
    )
  }
}

export default Login