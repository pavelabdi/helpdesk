import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import TicketEntry from '../TicketEntry'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import './style.css'
const uuidv1 = require('uuid/v1')
//import 'bootstrap-table/dist/bootstrap-table.min.js'
//import 'jquery/dist/jquery.min.js'

const token = localStorage.token

class TicketOverview extends Component {
    state = {
        ticket: [],
        histories: [],
        comments: [],
        comment: '',
        history: true,
        redirectToAllTickets: false,
        redirectToEdit: false,
        redirectToFeedback: false
    }

    handleChange = (event) => {
        event.preventDefault()
        this.setState({ [event.target.name]: event.target.value })
    }

    addComment = () => {
        const { params } = this.props.match
        fetch(`http://localhost:8080/tickets/${params.id}/comment`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                text: this.state.comment
            })
        }).then(() => {
            this.performRequest()
            this.setState({
               comment: ''
            })
        });
    }

    performRequest() {
        const { params } = this.props.match
        let request = ''
        request = `http://localhost:8080/tickets/${params.id}`
        fetch(request, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                }
            })
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    ticket: data
                })
            })
        request = `http://localhost:8080/tickets/${params.id}/history`
        fetch(request, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                }
            })
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    histories: data
                })
            })
        request = `http://localhost:8080/tickets/${params.id}/comments`
        fetch(request, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                }
            })
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    comments: data
                })
            })
    }

    componentDidMount() {
        this.performRequest()
    }

    renderRedirectToAllTickets = () => {
        if (this.state.redirectToAllTickets) {
            return <Redirect to='/tickets' />
        }
    }

    renderRedirectToEdit = () => {
        const { params } = this.props.match
        if (this.state.redirectToEdit) {
            return <Redirect to={`/tickets/${params.id}/edit`} />
        }
    }

    renderRedirectToFeedback = () => {
        const { params } = this.props.match
        if (this.state.redirectToFeedback) {
            return <Redirect to={`/tickets/${params.id}/feedback`} />
        }
    }

    render() {
        let value = this.state.history ? this.state.histories : this.state.comments
        const filled = "btn btn-lg btn-primary"
        const empty = "btn btn-lg btn-outline-primary"
        return (
            <>
                {this.renderRedirectToAllTickets()}
                {this.renderRedirectToEdit()}
                {this.renderRedirectToFeedback()}
                <button onClick={this.redirectToAllTickets} className="btn btn-lg btn-success float-left" style={{ width: '20%', margin: '10px' }}>Ticket List</button>
                <h2>Ticket ({this.state.ticket.id}) - {this.state.ticket.name}</h2>
                <label style={{width: '80%'}}>Created on: {this.state.ticket.createdDate}</label>
                <label style={{width: '80%'}}>Status: {this.state.ticket.state}</label>
                <label style={{width: '80%'}}>Category: {this.state.ticket.category}</label>
                <label style={{width: '80%'}}>Urgency: {this.state.ticket.urgency}</label>
                <label style={{width: '80%'}}>Desired resolution date: {this.state.ticket.dueDate}</label>
                <label style={{width: '80%'}}>Owner: {this.state.ticket.owner}</label>
                <label style={{width: '80%'}}>Approver: {this.state.ticket.approver}</label>
                <label style={{width: '80%'}}>Assignee: {this.state.ticket.assignee}</label>
                <label style={{width: '80%'}}>Description: {this.state.ticket.description}</label>
                <button onClick={this.redirectToEdit} className="btn btn-lg btn-success float-right" style={{ width: '20%', margin: '10px' }}>Edit</button>
                <button onClick={this.redirectToFeedback} className="btn btn-lg btn-success float-right" style={{ width: '20%', margin: '10px' }}>Leave Feedback</button>
                <div className='tickets__container'>
                    <button onClick={this.handleClickLeft} className={this.state.history ? filled : empty} style={{ width: '40%', margin: '10px' }}>History</button>
                    <button onClick={this.handleClickRight} className={this.state.history ? empty : filled} style={{ width: '40%', margin: '10px' }}>Comments</button>
                </div>
                <div className="input-group" style={{ width: '20%', margin: '10px' }}>
                    <input type="text" className="form-control" placeholder="Search"></input>
                    <div className="input-group-btn">
                        <button className="btn btn-default" type="submit">
                            <i className="fas fa-search"></i>
                        </button>
                    </div>
                </div>  

                <table className="table table-bordered table-hover" style={{ margin: '10px 10px 10px 10px', width: '80%' }}>
                    <thead className="thead-light">
                        {this.state.history ? (
                        <tr>
                            <th scope="col" data-sortable="true">Date</th>
                            <th scope="col">User</th>
                            <th scope="col">Action</th>
                            <th scope="col">Description</th>
                        </tr>
                        ) : (
                        <tr>
                            <th scope="col" data-sortable="true">Date</th>
                            <th scope="col">User</th>
                            <th scope="col">Comment</th>
                            </tr>
                        )}
                    </thead>
                    <tbody>
                        {Object.keys(value).map((el) => {
                            return (
                                <TicketEntry onClick={this.handleClick} type={this.state.history ? 'history' : 'comment'} arr={value[el]} key={uuidv1()}/>
                            )
                        })}
                    </tbody>
                </table>

                {this.state.history ? 
                    null : (
                        <label style={{ margin: '10px 10px 10px 10px', width: '80%' }}>Comment:
                            <textarea type="text" className="form-control" name="comment" value={this.state.comment} onChange={this.handleChange} />
                            <button type="submit" className="btn btn-light" onClick={this.addComment}>Add comment</button>
                        </label>
                    )
                }
            </>
        );
    }

    redirectToAllTickets = () => {
        this.setState({
            redirectToAllTickets: true
        });
    }

    redirectToEdit = () => {
        this.setState({
            redirectToEdit: true
        });
    }

    redirectToFeedback = () => {
        this.setState({
            redirectToFeedback: true
        });
    }

    handleClickRight = () => {
        console.log('---', 2)
        this.setState({
            history: false
        })
    }

    handleClickLeft = () => {
        console.log('---', 3)
        this.setState({
            history: true
        })
    }
}

export default TicketOverview