import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import TicketEntry from '../TicketEntry'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import './style.css'
const uuidv1 = require('uuid/v1')
//import 'bootstrap-table/dist/bootstrap-table.min.js'
//import 'jquery/dist/jquery.min.js'

const token = localStorage.token

class Tickets extends Component {
    state = {
        value1: [],
        value2: [],
        alltickets: true,
        redirect: false
    }

    performRequest = () => {
        let request = ''
        request = 'http://localhost:8080/tickets/all'
        fetch(request, {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': `Bearer ${token}`
            }})
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    value1: data
                })
            })
        request = 'http://localhost:8080/tickets/my'
        fetch(request, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            }})
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    value2: data
                })
            })
    }

    componentDidMount() {
        this.performRequest()
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/create' />
        }
    }

    handleClick = (id, action) => {
        console.log(id)
        console.log(action)
        fetch(`http://localhost:8080/tickets/${id}/${action}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                //action: text
            })
        }).then(() => {
            this.performRequest()
        });
        
    }

    redirectToTarget = () => {
        this.setState({
            redirect: true
        });
    }

    handleClickRight = () => {
        this.setState({
            alltickets: false
        })
    }

    handleClickLeft = () => {
        this.setState({
            alltickets: true
        })
    }

    render() {
        let value = this.state.alltickets ? this.state.value1 : this.state.value2
        const filled = "btn btn-lg btn-primary"
        const empty = "btn btn-lg btn-outline-primary"
        return (
            <>
                {this.renderRedirect()}
                <button onClick={this.redirectToTarget} className="btn btn-lg btn-success float-right" style={{ width: '20%', margin: '10px' }}>Create New Ticket</button>
                <div className='tickets__container'>
                    <button onClick={this.handleClickLeft} className={this.state.alltickets ? filled : empty} style={{ width: '40%', margin: '10px' }}>All Tickets</button>
                    <button onClick={this.handleClickRight} className={this.state.alltickets ? empty : filled} style={{ width: '40%', margin: '10px' }}>My Tickets</button>
                </div>
                <div className="input-group" style={{ width: '20%', margin: '10px' }}>
                    <input type="text" className="form-control" placeholder="Search"></input>
                    <div className="input-group-btn">
                        <button className="btn btn-default" type="submit">
                            <i className="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                <table className="table table-bordered table-hover" style={{ margin: '10px 10px 10px 10px', width: '80%' }}>
                    <thead className="thead-light">
                        <tr>
                            <th scope="col" data-sortable="true">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Desired Date</th>
                            <th scope="col">Urgency</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Object.keys(value).map((el) => {
                            return (
                                <TicketEntry onClick={this.handleClick} type='ticket' arr={value[el]} key={uuidv1()} onUpdate={this.performRequest}/>
                            )
                        })}
                    </tbody>
                </table>

                <table data-toggle="table" data-url="https://api.github.com/users/wenzhixin/repos?type=owner&sort=full_name&direction=asc&per_page=100&page=1"
                    data-sort-name="stargazers_count" data-sort-order="desc">
                    <thead>
                        <tr>
                            <th data-field="name" data-sortable="true"> Name </th>
                            <th data-field="stargazers_count" data-sortable="true"> Stars </th>
                            <th data-field="forks_count" data-sortable="true"> Forks </th>
                            <th data-field="description" data-sortable="true"> Description </th>
                        </tr>
                    </thead>
                </table>
            </>
        );
    }
}

export default Tickets