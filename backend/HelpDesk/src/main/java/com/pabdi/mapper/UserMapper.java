package com.pabdi.mapper;

import com.pabdi.dto.UserDto;
import com.pabdi.entity.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final BCryptPasswordEncoder passwordEncoder;

    public UserMapper(BCryptPasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }

    public User toEntity(UserDto dto){
        User newUser = new User();
        newUser.setUserFirstName(dto.getFirstName());
        newUser.setUserLastName(dto.getLastName());
        newUser.setUserEmail(dto.getEmail());
        newUser.setUserPassword(passwordEncoder.encode(dto.getPassword()));
        return newUser;
    }

    public UserDto toDto(User entity){
        UserDto newUserDto = new UserDto();
        newUserDto.setFirstName(entity.getUserFirstName());
        newUserDto.setLastName(entity.getUserLastName());
        newUserDto.setEmail(entity.getUserEmail());
        //newUserDto.setPassword(entity.getUserPassword());
        return newUserDto;
    }
}
