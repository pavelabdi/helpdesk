package com.pabdi.mapper;

import com.pabdi.dto.CommentDto;
import com.pabdi.entity.Comment;
import com.pabdi.entity.Ticket;
import com.pabdi.entity.User;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class CommentMapper {

    private final String DATE_FORMAT = "MMM dd, yyyy HH:mm:ss";

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

    public CommentMapper(){
    }

    public Comment toEntity(String text, Ticket ticket, User user){
        Comment newComment = new Comment();
        newComment.setCommentDate(LocalDateTime.now());
        newComment.setCommentText(text);
        newComment.setCommentTicket(ticket);
        newComment.setCommentUser(user);
        return newComment;
    }

    public CommentDto toDto(Comment entity){
        CommentDto newCommentDto = new CommentDto();
        newCommentDto.setText(entity.getCommentText());
        newCommentDto.setDate(entity.getCommentDate().format(formatter));
        newCommentDto.setUser(
                String.format("%s %s",
                        entity.getCommentUser().getUserFirstName(),
                        entity.getCommentUser().getUserLastName()));
        return newCommentDto;
    }
}
