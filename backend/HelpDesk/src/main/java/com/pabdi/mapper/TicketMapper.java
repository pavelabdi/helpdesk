package com.pabdi.mapper;

import com.pabdi.dto.TicketDto;
import com.pabdi.entity.Action;
import com.pabdi.entity.Role;
import com.pabdi.entity.State;
import com.pabdi.entity.Ticket;
import com.pabdi.entity.Urgency;
import com.pabdi.entity.User;
import com.pabdi.service.CategoryService;
import com.pabdi.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

@Component
public class TicketMapper {

    private final CategoryService categoryService;

    private final UserService userService;

    private final String DATE_FORMAT_TO_DTO = "dd/MM/yyyy";

    private final String DATE_FORMAT_TO_ENTITY = "yyyy-MM-dd";

    private DateTimeFormatter formatterToDto = DateTimeFormatter.ofPattern(DATE_FORMAT_TO_DTO);

    private DateTimeFormatter formatterToEntity = DateTimeFormatter.ofPattern(DATE_FORMAT_TO_ENTITY);

    public TicketMapper(@Qualifier("categoryService") CategoryService categoryService,
                        @Qualifier("userService") UserService userService) {
        this.categoryService = categoryService;
        this.userService = userService;
    }

    private List<String> getActions(Ticket ticket){
        List<String> actions = new LinkedList<>();
        if (ticket.getTicketOwner().getUserRole() == userService.getCurrentUser().getUserRole() &&
                (ticket.getTicketState() == State.DRAFT || ticket.getTicketState() == State.DECLINED)){
            actions.add("Submit");
            actions.add("Cancel");
        } else if (ticket.getTicketOwner().getUserRole() == Role.EMPLOYEE &&
                userService.getCurrentUser().getUserRole() == Role.MANAGER &&
                ticket.getTicketState() == State.NEW) {
            actions.add("Approve");
            actions.add("Decline");
            actions.add("Cancel");
        } else if ((userService.getCurrentUser().getUserRole() == Role.ENGINEER &&
                ticket.getTicketState() == State.APPROVED)) {
            actions.add("Assign_to_Me");
            actions.add("Cancel");
        } else if ((userService.getCurrentUser().getUserRole() == Role.ENGINEER &&
                ticket.getTicketState() == State.IN_PROGRESS)) {
            actions.add("Done");
        }
        return actions;
    }

    public Ticket toEntity(TicketDto dto, User user, Action action) {
        Ticket newTicket = new Ticket();
        newTicket.setTicketCategory(categoryService.getByName(dto.getCategory()));
        newTicket.setTicketName(dto.getName());
        newTicket.setTicketDescription(dto.getDescription());
        newTicket.setTicketUrgency(Urgency.valueOf(dto.getUrgency().toUpperCase()));
        newTicket.setTicketCreatedDate(LocalDateTime.now());
        newTicket.setTicketOwner(user);
        newTicket.setTicketState(action.getState());
        newTicket.setTicketDueDate(LocalDateTime.of(LocalDate.parse(dto.getDueDate(), formatterToEntity), LocalTime.of(0, 0)));
        return newTicket;
    }

    public TicketDto toDto(Ticket entity) {
        TicketDto newTicketDto = new TicketDto();
        newTicketDto.setId(entity.getTicketId());
        newTicketDto.setName(entity.getTicketName());
        newTicketDto.setDueDate(entity.getTicketDueDate().format(formatterToDto));
        newTicketDto.setState(entity.getTicketState().toString());
        newTicketDto.setUrgency(entity.getTicketUrgency().toString());
        newTicketDto.setCreatedDate(entity.getTicketCreatedDate().format(formatterToDto));
        newTicketDto.setOwner(
                String.format("%s %s",
                        entity.getTicketOwner().getUserFirstName(),
                        entity.getTicketOwner().getUserLastName()));
        newTicketDto.setApprover(
                entity.getTicketApprover() != null ?
                        String.format("%s %s",
                                entity.getTicketApprover().getUserFirstName(),
                                entity.getTicketApprover().getUserLastName())
                        : "");
        newTicketDto.setAssignee(
                entity.getTicketAssignee() != null ?
                        String.format("%s %s",
                                entity.getTicketAssignee().getUserFirstName(),
                                entity.getTicketAssignee().getUserLastName())
                        : "");
        newTicketDto.setDescription(entity.getTicketDescription());
        newTicketDto.setCategory(entity.getTicketCategory().getCategoryName());
        newTicketDto.setActions(getActions(entity));
        return newTicketDto;
    }
}
