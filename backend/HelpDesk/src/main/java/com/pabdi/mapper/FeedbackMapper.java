package com.pabdi.mapper;

import com.pabdi.dto.FeedbackDto;
import com.pabdi.entity.Feedback;
import com.pabdi.entity.Ticket;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class FeedbackMapper {

    public FeedbackMapper(){
    }

    public Feedback toEntity(FeedbackDto dto, Ticket ticket){
        Feedback newFeedback = new Feedback();
        newFeedback.setFeedbackRate(dto.getRate());
        newFeedback.setFeedbackText(dto.getText());
        newFeedback.setFeedbackTicket(ticket);
        newFeedback.setFeedbackDate(LocalDateTime.now());
        return newFeedback;
    }

    public FeedbackDto toDto(Feedback entity){
        FeedbackDto newFeedbackDto = new FeedbackDto();
        newFeedbackDto.setText(entity.getFeedbackText());
        newFeedbackDto.setRate(entity.getFeedbackRate());
        //return newFeedbackDto;

        return FeedbackDto.FeedbackDtoBuilder.aFeedbackDto().withRate(entity.getFeedbackRate()).build();
    }
}
