package com.pabdi.mapper;

import com.pabdi.dto.HistoryDto;
import com.pabdi.entity.Action;
import com.pabdi.entity.History;
import com.pabdi.entity.State;
import com.pabdi.entity.Ticket;
import com.pabdi.entity.User;
import com.pabdi.service.TicketService;
import com.pabdi.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class HistoryMapper {

    private final String DATE_FORMAT = "MMM dd, yyyy HH:mm:ss";

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

    public HistoryMapper(){
    }

    public History toEntity(Ticket ticket, User user, Action action){
        History newHistory = new History();
        State prevState = ticket.getTicketState();
        State nextState = action.getState();
        newHistory.setHistoryAction(action.getAction());
        newHistory.setHistoryDescription(String.format(action.getDescription(), prevState, nextState));
        newHistory.setHistoryDate(LocalDateTime.now());
        newHistory.setHistoryTicket(ticket);
        newHistory.setHistoryUser(user);
        return newHistory;
    }

    public HistoryDto toDto(History entity){
        HistoryDto newHistoryDto = new HistoryDto();
        newHistoryDto.setAction(entity.getHistoryAction());
        newHistoryDto.setDescription(entity.getHistoryDescription());
        newHistoryDto.setDate(entity.getHistoryDate().format(formatter));
        newHistoryDto.setUser(
                String.format("%s %s",
                        entity.getHistoryUser().getUserFirstName(),
                        entity.getHistoryUser().getUserLastName()));
        return newHistoryDto;
    }
}
