package com.pabdi.dto;

public class FeedbackDto {

    private Byte rate;

    private String text;

    public FeedbackDto() {
    }

    public Byte getRate() {
        return rate;
    }

    public void setRate(Byte rate) {
        this.rate = rate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public static final class FeedbackDtoBuilder {
        private Byte rate;
        private String text;

        private FeedbackDtoBuilder() {
        }

        public static FeedbackDtoBuilder aFeedbackDto() {
            return new FeedbackDtoBuilder();
        }

        public FeedbackDtoBuilder withRate(Byte rate) {
            this.rate = rate;
            return this;
        }

        public FeedbackDtoBuilder withText(String text) {
            this.text = text;
            return this;
        }

        public FeedbackDto build() {
            FeedbackDto feedbackDto = new FeedbackDto();
            feedbackDto.setRate(rate);
            feedbackDto.setText(text);
            return feedbackDto;
        }
    }
}
