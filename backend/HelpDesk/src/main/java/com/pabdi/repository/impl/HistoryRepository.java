package com.pabdi.repository.impl;

import com.pabdi.entity.History;
import com.pabdi.entity.Ticket;
import com.pabdi.entity.User;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class HistoryRepository extends AbstractRepository<Long, History> {

    public HistoryRepository() {
    }

    public Optional<History> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<List<History>> findByTicket(Ticket ticket) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM History h WHERE h.historyTicket =:ticket", History.class)
                .setParameter("ticket", ticket)
                .getResultList());
    }

    public Optional<List<History>> findByUser(User user) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM History h WHERE h.historyUser =:user", History.class)
                .setParameter("user", user)
                .getResultList());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<History>> findAll() {
        return Optional.ofNullable(getAll());
    }

    public History save(History history) {
        return persist(history);
    }

    public History change(History history) { return update(history); }
}
