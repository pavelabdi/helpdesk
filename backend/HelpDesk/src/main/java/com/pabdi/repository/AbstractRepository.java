package com.pabdi.repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractRepository<PK extends Serializable, T> {

    private final Class<T> persistentClass;

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public AbstractRepository() {
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    protected EntityManager getEntityManager(){
        return this.entityManager;
    }


    protected T getByKey(PK key) {
        return entityManager.find(persistentClass, key);
    }

    protected T persist(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    protected T update(T entity) {
        return entityManager.merge(entity);
    }

    protected void delete(T entity) {
        entityManager.remove(entity);
    }

    protected List<T> getAll() {
        return entityManager.createQuery("FROM " + persistentClass.getSimpleName(), persistentClass)
                .getResultList();
    }
}