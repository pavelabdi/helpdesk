package com.pabdi.repository.impl;

import com.pabdi.entity.Attachment;
import com.pabdi.entity.Ticket;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AttachmentRepository extends AbstractRepository<Long, Attachment> {

    public AttachmentRepository() {
    }

    public Optional<Attachment> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<Attachment> findAttachmentByName(String name) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Attachment a WHERE a.attachmentName =:name", Attachment.class)
                .setParameter("name", name)
                .getSingleResult());
    }

    public Optional<List<Attachment>> findByTicket(Ticket ticket) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Comment a WHERE a.attachmentTicket =:ticket", Attachment.class)
                .setParameter("ticket", ticket)
                .getResultList());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<Attachment>> findAll() {
        return Optional.of(getAll());
    }

    public Attachment save(Attachment attachment) {
        return persist(attachment);
    }

    public Attachment change(Attachment attachment) {
        return update(attachment);
    }
}
