package com.pabdi.repository.impl;

import com.pabdi.entity.User;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository extends AbstractRepository<Long, User> {

    public UserRepository(){
    }

    public Optional<User> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<User> findByEmail(String email) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM User u WHERE u.userEmail =:email", User.class)
                .setParameter("email", email)
                .getSingleResult());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<User>> findAll() {
        return Optional.of(getAll());
    }

    public User save(User user) {
        return persist(user);
    }

    public User change(User user) { return update(user); }
}
