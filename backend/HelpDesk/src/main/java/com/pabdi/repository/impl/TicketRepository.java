package com.pabdi.repository.impl;

import com.pabdi.entity.*;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TicketRepository extends AbstractRepository<Long, Ticket> {

    public TicketRepository() {
    }

    public List<Ticket> findForEmployee(User user) {
        return getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketOwner =:user", Ticket.class)
                .setParameter("user", user)
                .getResultList();
    }

    public List<Ticket> findForManager(User user) {
        return getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketOwner =:user OR " +
                        "t.ticketOwner.userRole = 0 AND t.ticketState = 1 OR " +
                        "t.ticketApprover = :user AND t.ticketState > 1", Ticket.class)
                .setParameter("user", user)
                .getResultList();
    }

    public List<Ticket> findForEngineer(User user) {
        return getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketOwner.userRole <> 2 AND t.ticketState = 2 OR " +
                        "t.ticketAssignee =:user AND t.ticketState > 4", Ticket.class)
                .setParameter("user", user)
                .getResultList();
    }

    public Optional<Ticket> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<Ticket> findByName(String name) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketName =:name", Ticket.class)
                .setParameter("name", name)
                .getSingleResult());
    }

    public List<Ticket> findByAssignee(User user) {
        return getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketAssignee =:user", Ticket.class)
                .setParameter("user", user)
                .getResultList();
    }

    public List<Ticket> findByOwner(User user) {
        return getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketOwner =:user", Ticket.class)
                .setParameter("user", user)
                .getResultList();
    }

    public List<Ticket> findByApprover(User user) {
        return getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketApprover =:user", Ticket.class)
                .setParameter("user", user)
                .getResultList();
    }

    public List<Ticket> findByState(State state) {
        return getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketState =:state", Ticket.class)
                .setParameter("state", state)
                .getResultList();
    }

    public Optional<List<Ticket>> findByCategory(Category category) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketCategory =:category", Ticket.class)
                .setParameter("category", category)
                .getResultList());
    }

    public Optional<List<Ticket>> findByUrgency(Urgency urgency) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Ticket t WHERE t.ticketUrgency =:urgency", Ticket.class)
                .setParameter("urgency", urgency)
                .getResultList());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<Ticket>> findAll() {
        return Optional.ofNullable(getAll());
    }

    public Ticket save(Ticket ticket) {
        return persist(ticket);
    }

    public Ticket change(Ticket ticket) { return update(ticket); }
}
