package com.pabdi.repository.impl;

import com.pabdi.entity.*;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class FeedbackRepository extends AbstractRepository<Long, Feedback> {

    public FeedbackRepository() {
    }

    public Optional<Feedback> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<Feedback> findByTicket(Ticket ticket) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Feedback f WHERE f.feedbackTicket =:ticket", Feedback.class)
                .setParameter("ticket", ticket)
                .getSingleResult());
    }

    public Optional<List<Feedback>> findByUser(User user) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Feedback f WHERE f.feedbackUser =:user", Feedback.class)
                .setParameter("user", user)
                .getResultList());
    }

    public Optional<List<Feedback>> findByRate(Byte rate) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Feedback f WHERE f.feedbackRate =:rate", Feedback.class)
                .setParameter("rate", rate)
                .getResultList());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<Feedback>> findAll() {
        return Optional.ofNullable(getAll());
    }

    public Feedback save(Feedback feedback) {
        return persist(feedback);
    }

    public Feedback change(Feedback feedback) { return update(feedback); }
}
