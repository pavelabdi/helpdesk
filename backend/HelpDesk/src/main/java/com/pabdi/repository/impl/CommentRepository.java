package com.pabdi.repository.impl;

import com.pabdi.entity.*;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CommentRepository extends AbstractRepository<Long, Comment> {

    public CommentRepository() {
    }

    public Optional<Comment> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<List<Comment>> findByTicket(Ticket ticket) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Comment c WHERE c.commentTicket =:ticket", Comment.class)
                .setParameter("ticket", ticket)
                .getResultList());
    }

    public Optional<List<Comment>> findByUser(User user) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Comment c WHERE c.commentUser =:user", Comment.class)
                .setParameter("user", user)
                .getResultList());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<Comment>> findAll() {
        return Optional.ofNullable(getAll());
    }

    public Comment save(Comment comment) {
        return persist(comment);
    }

    public Comment change(Comment comment) { return update(comment); }
}
