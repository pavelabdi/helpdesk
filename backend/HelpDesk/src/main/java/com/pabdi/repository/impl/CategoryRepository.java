package com.pabdi.repository.impl;

import com.pabdi.entity.Category;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CategoryRepository extends AbstractRepository<Long, Category> {

    public CategoryRepository() {
    }

    public Optional<Category> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<Category> findAttachmentByName(String name) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Category c WHERE c.categoryName =:name", Category.class)
                .setParameter("name", name)
                .getSingleResult());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<Category>> findAll() {
        return Optional.of(getAll());
    }

    public Category save(Category attachment) {
        return persist(attachment);
    }

    public Category change(Category attachment) {
        return update(attachment);
    }
}
