package com.pabdi.entity;

public enum Action {
    SAVE_AS_DRAFT(),
    CREATE("Ticket is created", "Ticket is created"),
    EDIT("Ticket is edited", "Ticket is edited"),
    SUBMIT("Ticket Status is changed", "Ticket Status is changed from [%s] to [%s]"),
    APPROVE("Ticket Status is changed", "Ticket Status is changed from [%s] to [%s]"),
    DECLINE("Ticket Status is changed", "Ticket Status is changed from [%s] to [%s]"),
    CANCEL("Ticket Status is changed", "Ticket Status is changed from [%s] to [%s]"),
    ASSIGN_TO_ME("Ticket Status is changed", "Ticket Status is changed from [%s] to [%s]"),
    DONE("Ticket Status is changed", "Ticket Status is changed from [%s] to [%s]"),
    ATTACH_FILE("File is attached", "File is attached: [%s]"),
    REMOVE_FILE("File is removed", "File is removed: [%s]"),
    LEAVE_FEEDBACK,
    VIEW_FEEDBACK;

    private State state;

    private String action;

    private String description;

    static {
        SAVE_AS_DRAFT.state = State.DRAFT;
        CREATE.state = State.NEW;
        SUBMIT.state = State.NEW;
        APPROVE.state = State.APPROVED;
        DECLINE.state = State.DECLINED;
        CANCEL.state = State.CANCELLED;
        ASSIGN_TO_ME.state = State.IN_PROGRESS;
        DONE.state = State.DONE;
    }

    Action(String action, String description){
        this.action = action;
        this.description = description;
    }

    Action(){

    }

    public State getState() {
        return state;
    }

    public String getAction(){
        return action;
    }

    public String getDescription() {
        return description;
    }
}
