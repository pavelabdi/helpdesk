package com.pabdi.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "attachments")
public class Feedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long feedbackId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User feedbackUser;

    @Column(name = "rate")
    private Byte feedbackRate;

    @Column(name = "date")
    private LocalDateTime feedbackDate;

    @Column(name = "text")
    private String feedbackText;

    @OneToOne
    @JoinColumn(name = "ticket_id")
    private Ticket feedbackTicket;

    public Feedback() {
    }

    public Long getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Long feedbackId) {
        this.feedbackId = feedbackId;
    }

    public User getFeedbackUser() {
        return feedbackUser;
    }

    public void setFeedbackUser(User feedbackUser) {
        this.feedbackUser = feedbackUser;
    }

    public Byte getFeedbackRate() {
        return feedbackRate;
    }

    public void setFeedbackRate(Byte feedbackRate) {
        this.feedbackRate = feedbackRate;
    }

    public LocalDateTime getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(LocalDateTime feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFeedbackText() {
        return feedbackText;
    }

    public void setFeedbackText(String feedbackText) {
        this.feedbackText = feedbackText;
    }

    public Ticket getFeedbackTicket() {
        return feedbackTicket;
    }

    public void setFeedbackTicket(Ticket feedbackTicket) {
        this.feedbackTicket = feedbackTicket;
    }
}
