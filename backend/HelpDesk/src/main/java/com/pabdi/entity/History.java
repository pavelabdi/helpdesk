package com.pabdi.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "histories")
public class History {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long historyId;

    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket historyTicket;

    @Column(name = "date")
    private LocalDateTime historyDate;

    @Column(name = "action")
    private String historyAction;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User historyUser;

    @Column(name = "description")
    private String historyDescription;

    public History() {
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public Ticket getHistoryTicket() {
        return historyTicket;
    }

    public void setHistoryTicket(Ticket historyTicket) {
        this.historyTicket = historyTicket;
    }

    public LocalDateTime getHistoryDate() {
        return historyDate;
    }

    public void setHistoryDate(LocalDateTime historyDate) {
        this.historyDate = historyDate;
    }

    public String getHistoryAction() {
        return historyAction;
    }

    public void setHistoryAction(String historyAction) {
        this.historyAction = historyAction;
    }

    public User getHistoryUser() {
        return historyUser;
    }

    public void setHistoryUser(User historyUser) {
        this.historyUser = historyUser;
    }

    public String getHistoryDescription() {
        return historyDescription;
    }

    public void setHistoryDescription(String historyDescription) {
        this.historyDescription = historyDescription;
    }
}
