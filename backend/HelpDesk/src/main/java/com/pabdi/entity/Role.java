package com.pabdi.entity;

public enum Role {
    EMPLOYEE,
    MANAGER,
    ENGINEER
}
