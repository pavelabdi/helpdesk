package com.pabdi.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Entity
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long ticketId;

    @Column(name = "name")
    //@NotEmpty
    private String ticketName;

    @Column(name = "description")
    private String ticketDescription;

    @Column(name = "created_on")
    private LocalDateTime ticketCreatedDate;

    @Column(name = "desired_resolution_date")
    private LocalDateTime ticketDueDate;

    @ManyToOne
    @JoinColumn(name = "assignee_id")
    private User ticketAssignee;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User ticketOwner;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "state_id")
    private State ticketState;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category ticketCategory;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "urgency_id")
    private Urgency ticketUrgency;

    @ManyToOne
    @JoinColumn(name = "approver_id")
    private User ticketApprover;

    public Ticket() {
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public String getTicketName() {
        return ticketName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

    public LocalDateTime getTicketCreatedDate() {
        return ticketCreatedDate;
    }

    public void setTicketCreatedDate(LocalDateTime ticketCreatedDate) {
        this.ticketCreatedDate = ticketCreatedDate;
    }

    public LocalDateTime getTicketDueDate() {
        return ticketDueDate;
    }

    public void setTicketDueDate(LocalDateTime ticketDueDate) {
        this.ticketDueDate = ticketDueDate;
    }

    public User getTicketAssignee() {
        return ticketAssignee;
    }

    public void setTicketAssignee(User ticketAssignee) {
        this.ticketAssignee = ticketAssignee;
    }

    public User getTicketOwner() {
        return ticketOwner;
    }

    public void setTicketOwner(User ticketOwner) {
        this.ticketOwner = ticketOwner;
    }

    public State getTicketState() {
        return ticketState;
    }

    public void setTicketState(State ticketState) {
        this.ticketState = ticketState;
    }

    public Category getTicketCategory() {
        return ticketCategory;
    }

    public void setTicketCategory(Category ticketCategory) {
        this.ticketCategory = ticketCategory;
    }

    public Urgency getTicketUrgency() {
        return ticketUrgency;
    }

    public void setTicketUrgency(Urgency ticketUrgency) {
        this.ticketUrgency = ticketUrgency;
    }

    public User getTicketApprover() {
        return ticketApprover;
    }

    public void setTicketApprover(User ticketApprover) {
        this.ticketApprover = ticketApprover;
    }
}
