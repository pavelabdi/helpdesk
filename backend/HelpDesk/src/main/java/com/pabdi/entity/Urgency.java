package com.pabdi.entity;

public enum Urgency {
    CRITICAL,
    HIGH,
    AVERAGE,
    LOW
}
