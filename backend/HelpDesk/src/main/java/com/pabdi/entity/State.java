package com.pabdi.entity;

public enum State {
    DRAFT,
    NEW,
    APPROVED,
    DECLINED,
    CANCELLED,
    IN_PROGRESS,
    DONE
}
