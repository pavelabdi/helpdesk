package com.pabdi.service;

import com.pabdi.entity.User;

import java.util.List;

public interface UserService {

    User getCurrentUser();

    List<User> getAll();

    User getById(Long id);

    User getByEmail(String email);

    User add(User user);

    User update(User user, Long id);
}

