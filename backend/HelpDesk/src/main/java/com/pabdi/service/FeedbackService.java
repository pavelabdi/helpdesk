package com.pabdi.service;

import com.pabdi.entity.*;

import java.util.List;

public interface FeedbackService {

    List<Feedback> getAll();

    Feedback getById(Long id);

    Feedback getByTicket(Ticket ticket);

    List<Feedback> getByUser(User user);

    List<Feedback> getByRate(Byte rate);

    Feedback add(Feedback feedback);

    Feedback update(Feedback feedback, Long id);
}
