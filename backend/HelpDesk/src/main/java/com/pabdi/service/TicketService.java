package com.pabdi.service;

import com.pabdi.entity.*;

import java.util.List;

public interface TicketService {

    List<Ticket> getAll();

    List<Ticket> getAllForUser();

    Ticket getById(Long id);

    Ticket getByName(String name);

    List<Ticket> getByAssignee(User user);

    List<Ticket> getByOwner(User user);

    List<Ticket> getByApprover(User user);

    List<Ticket> getByState(State state);

    List<Ticket> getByCategory(Category category);

    List<Ticket> getByUrgency(Urgency urgency);

    Ticket add(Ticket ticket, Comment comment, History history);

    Ticket update(Ticket ticket, Long id);
}
