package com.pabdi.service;

import com.pabdi.entity.*;

import java.util.List;

public interface HistoryService {

    List<History> getAll();

    History getById(Long id);

    List<History> getByTicket(Ticket ticket);

    List<History> getByUser(User user);

    History add(History history);

    History update(History history, Long id);
}
