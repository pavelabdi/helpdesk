package com.pabdi.service;

import com.pabdi.entity.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(Long id);

    Category getByName(String name);

    Category add(Category category);

    Category update(Category category, Long id);
}
