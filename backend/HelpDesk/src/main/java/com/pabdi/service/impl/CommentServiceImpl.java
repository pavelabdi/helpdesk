package com.pabdi.service.impl;

import com.pabdi.entity.*;
import com.pabdi.repository.impl.CommentRepository;
import com.pabdi.service.CommentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("commentService")
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> getAll() {
        return commentRepository.findAll()
                .orElseThrow(() -> new NoResultException("No comments found"));
    }

    @Override
    public Comment getById(Long id) {
        return commentRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No comment found with id " + id));
    }

    @Override
    public List<Comment> getByTicket(Ticket ticket) {
        return commentRepository.findByTicket(ticket)
                .orElseThrow(() -> new NoResultException("No comments found for ticket id " + ticket.getTicketId()));
    }

    @Override
    public List<Comment> getByUser(User user) {
        return commentRepository.findByUser(user)
                .orElseThrow(() -> new NoResultException("No comments found for user id " + user.getUserId()));
    }

    @Transactional
    @Override
    public Comment add(Comment comment) {
        return commentRepository.save(comment);
    }

    @Transactional
    @Override
    public Comment update(Comment newComment, Long id) {
        return commentRepository.findById(id)
                .map(comment -> {
                    comment.setCommentTicket(newComment.getCommentTicket());
                    comment.setCommentDate(newComment.getCommentDate());
                    comment.setCommentText(newComment.getCommentText());
                    commentRepository.change(comment);
                    return comment;
                })
                .orElseGet(() -> commentRepository.save(newComment));
    }
}
