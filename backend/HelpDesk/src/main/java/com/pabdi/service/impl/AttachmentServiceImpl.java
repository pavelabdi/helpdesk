package com.pabdi.service.impl;

import com.pabdi.entity.Attachment;
import com.pabdi.entity.Ticket;
import com.pabdi.repository.impl.AttachmentRepository;
import com.pabdi.service.AttachmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("attachmentService")
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;

    public AttachmentServiceImpl(AttachmentRepository attachmentRepository){
        this.attachmentRepository = attachmentRepository;
    }

    @Override
    public List<Attachment> getAll() {
        return attachmentRepository.findAll()
                .orElseThrow(() -> new NoResultException("No attachments found"));
    }

    @Override
    public Attachment getById(Long id) {
        return attachmentRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No attachments found with id " + id));
    }

    @Override
    public Attachment getByName(String name) {
        return attachmentRepository.findAttachmentByName(name)
                .orElseThrow(() -> new NoResultException("No attachment found with name " + name));
    }

    @Override
    public List<Attachment> getByTicket(Ticket ticket) {
        return attachmentRepository.findByTicket(ticket)
                .orElseThrow(() -> new NoResultException("No attachments found for ticket id " + ticket.getTicketId()));
    }

    @Transactional
    @Override
    public Attachment add(Attachment attachment){
        return attachmentRepository.save(attachment);
    }

    @Transactional
    @Override
    public Attachment update(Attachment newAttachment, Long id) {
        return attachmentRepository.findById(id)
                .map(attachment -> {
                    attachment.setAttachmentTicket(newAttachment.getAttachmentTicket());
                    attachment.setAttachmentName(newAttachment.getAttachmentName());
                    attachment.setAttachmentBlob(newAttachment.getAttachmentBlob());
                    attachmentRepository.change(attachment);
                    return attachment;
                })
                .orElseGet(() -> attachmentRepository.save(newAttachment));
    }
}
