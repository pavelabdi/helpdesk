package com.pabdi.service.impl;

import com.pabdi.entity.*;
import com.pabdi.repository.impl.CommentRepository;
import com.pabdi.repository.impl.HistoryRepository;
import com.pabdi.repository.impl.TicketRepository;
import com.pabdi.service.CommentService;
import com.pabdi.service.HistoryService;
import com.pabdi.service.TicketService;
import com.pabdi.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;
import java.util.EnumMap;
import java.util.List;
import java.util.function.Function;

@Service("ticketService")
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;

    private final CommentRepository commentRepository;

    private final HistoryRepository historyRepository;

    private final UserService userService;

    private EnumMap<Role, Function<User,List<Ticket>>> roleFunctionMap = new EnumMap<>(Role.class);

    public TicketServiceImpl(TicketRepository ticketRepository,
                             @Qualifier("userService") UserService userService,
                             CommentRepository commentRepository,
                             HistoryRepository historyRepository) {
        this.ticketRepository = ticketRepository;
        this.commentRepository = commentRepository;
        this.historyRepository = historyRepository;
        this.userService = userService;
        roleFunctionMap.put(Role.EMPLOYEE, ticketRepository::findForEmployee);
        roleFunctionMap.put(Role.ENGINEER, ticketRepository::findForEngineer);
        roleFunctionMap.put(Role.MANAGER, ticketRepository::findForManager);
    }

    @Override
    public List<Ticket> getAllForUser(){
        User loggedInUser = userService.getCurrentUser();
        return roleFunctionMap.get(loggedInUser.getUserRole()).apply(loggedInUser);
    }

    @Override
    public List<Ticket> getAll() {
        return ticketRepository.findAll()
                .orElseThrow(() -> new NoResultException("No tickets found"));
    }

    @Override
    public Ticket getById(Long id) {
        return ticketRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No ticket found with id " + id));
    }

    @Override
    public Ticket getByName(String name) {
        return ticketRepository.findByName(name)
                .orElseThrow(() -> new NoResultException("No ticket found with name " + name));
    }

    @Override
    public List<Ticket> getByAssignee(User user){
        return ticketRepository.findByAssignee(user);
    }

    @Override
    public List<Ticket> getByOwner(User user){
        return ticketRepository.findByOwner(user);
    }

    @Override
    public List<Ticket> getByApprover(User user){
        return ticketRepository.findByApprover(user);
    }

    @Override
    public List<Ticket> getByState(State state){
        return ticketRepository.findByState(state);
    }

    @Override
    public List<Ticket> getByCategory(Category category){
        return ticketRepository.findByCategory(category)
                .orElseThrow(() -> new NoResultException("No tickets found with category id " + category));
    }

    @Override
    public List<Ticket> getByUrgency(Urgency urgency){
        return ticketRepository.findByUrgency(urgency)
                .orElseThrow(() -> new NoResultException("No tickets found with urgency id " + urgency));
    }

    @Transactional
    @Override
    public Ticket add(Ticket ticket, Comment comment, History history) {
        commentRepository.save(comment);
        historyRepository.save(history);
        return ticketRepository.save(ticket);
    }

    @Transactional
    @Override
    public Ticket update(Ticket newTicket, Long id) {
        return ticketRepository.findById(id)
                .map(ticket -> {
                    ticket.setTicketName(newTicket.getTicketName());
                    ticket.setTicketDescription(newTicket.getTicketDescription());
                    ticket.setTicketCreatedDate(newTicket.getTicketCreatedDate());
                    ticket.setTicketDueDate(newTicket.getTicketDueDate());
                    ticket.setTicketAssignee(newTicket.getTicketAssignee());
                    ticket.setTicketOwner(newTicket.getTicketOwner());
                    ticket.setTicketState(newTicket.getTicketState());
                    ticket.setTicketCategory(newTicket.getTicketCategory());
                    ticket.setTicketUrgency(newTicket.getTicketUrgency());
                    ticket.setTicketApprover(newTicket.getTicketApprover());
                    ticketRepository.change(ticket);
                    return ticket;
                })
                .orElseGet(() -> ticketRepository.save(newTicket));
    }
}
