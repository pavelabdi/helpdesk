package com.pabdi.service.impl;

import com.pabdi.entity.Category;
import com.pabdi.repository.impl.CategoryRepository;
import com.pabdi.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository){
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll()
                .orElseThrow(() -> new NoResultException("No categories found"));
    }

    @Override
    public Category getById(Long id) {
        return categoryRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No category found with id " + id));
    }

    @Override
    public Category getByName(String name) {
        return categoryRepository.findAttachmentByName(name)
                .orElseThrow(() -> new NoResultException("No category found with name " + name));
    }

    @Transactional
    @Override
    public Category add(Category category){
        return categoryRepository.save(category);
    }

    @Transactional
    @Override
    public Category update(Category newCategory, Long id) {
        return categoryRepository.findById(id)
                .map(category -> {
                    category.setCategoryName(newCategory.getCategoryName());
                    categoryRepository.change(category);
                    return category;
                })
                .orElseGet(() -> categoryRepository.save(newCategory));
    }
}
