package com.pabdi.service;

import com.pabdi.entity.*;

import java.util.List;

public interface AttachmentService {

    List<Attachment> getAll();

    Attachment getById(Long id);

    Attachment getByName(String name);

    List<Attachment> getByTicket(Ticket ticket);

    Attachment add(Attachment entity);

    Attachment update(Attachment entity, Long id);
}
