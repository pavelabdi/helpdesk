package com.pabdi.service.impl;

import com.pabdi.entity.Action;
import com.pabdi.entity.State;
import com.pabdi.entity.Ticket;
import com.pabdi.entity.User;
import com.pabdi.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Locale;

@Service
public class MailServiceImpl {

    private static final String ENCODING = "utf-8";

    private final JavaMailSender mailSender;

    private final TemplateEngine htmlTemplateEngine;

    private final UserService userService;

    public MailServiceImpl(JavaMailSender mailSender,
                           TemplateEngine htmlTemplateEngine,
                           @Qualifier("userService") UserService userService){
        this.mailSender = mailSender;
        this.htmlTemplateEngine = htmlTemplateEngine;
        this.userService = userService;
    }

    public void sendAcceptableMail(Ticket ticket, Action action) {
        State prevState = ticket.getTicketState();
        Context context = new Context(Locale.US);
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, ENCODING);
        User user = userService.getCurrentUser();
        String[] recipientEmail = {"jstep@yopmail.com"};
        String subject = "";
        String template = "";
        if (action == Action.SUBMIT || action == Action.CREATE) {
            context.setVariable("ticketId", ticket.getTicketId().toString());
            template = "html/ticketSubmit";
            subject = "New ticket for approval";
        } else if (action == Action.APPROVE){
            context.setVariable("ticketId", ticket.getTicketId().toString());
            template = "html/ticketApprove";
            subject = "Ticket was approved";
        } else if (action == Action.DECLINE){
            context.setVariable("ticketId", ticket.getTicketId().toString());
            context.setVariable("firstName", user.getUserFirstName());
            context.setVariable("lastName", user.getUserLastName());
            template = "html/ticketDecline";
            subject = "Ticket was declined";
        } else if (action == Action.CANCEL && prevState == State.NEW){
            context.setVariable("ticketId", ticket.getTicketId().toString());
            context.setVariable("firstName", user.getUserFirstName());
            context.setVariable("lastName", user.getUserLastName());
            template = "html/ticketCancelFromNew";
            subject = "Ticket was cancelled";
        } else if (action == Action.CANCEL && prevState == State.APPROVED){
            context.setVariable("ticketId", ticket.getTicketId().toString());
            template = "html/ticketCancelFromApproved";
            subject = "Ticket was cancelled";
        } else if (action == Action.DONE) {
            context.setVariable("ticketId", ticket.getTicketId().toString());
            context.setVariable("firstName", user.getUserFirstName());
            context.setVariable("lastName", user.getUserLastName());
            template = "html/ticketDone";
            subject = "Ticket was done";
        } else if (action == Action.LEAVE_FEEDBACK) {
            context.setVariable("ticketId", ticket.getTicketId().toString());
            context.setVariable("firstName", user.getUserFirstName());
            context.setVariable("lastName", user.getUserLastName());
            template = "html/ticketFeedback";
            subject = "Feedback was provided";
        }

        try {
            helper.setTo(recipientEmail);
            String htmlContext = this.htmlTemplateEngine.process(template, context);
            helper.setText(htmlContext, true);
            helper.setSubject(subject);
            this.mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
