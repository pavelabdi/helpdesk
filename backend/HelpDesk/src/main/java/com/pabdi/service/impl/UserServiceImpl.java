package com.pabdi.service.impl;

import com.pabdi.entity.User;
import com.pabdi.repository.impl.UserRepository;
import com.pabdi.service.UserService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll()
                .orElseThrow(() -> new NoResultException("No users found"));
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No user found with id " + id));
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new NoResultException("No user found with email " + email));
    }

    @Transactional
    @Override
    public User add(User user) {
        return userRepository.save(user);
    }

    @Transactional
    @Override
    public User update(User newUser, Long id) {
        return userRepository.findById(id)
                .map(user -> {
                    user.setUserFirstName(newUser.getUserFirstName());
                    user.setUserLastName(newUser.getUserLastName());
                    user.setUserRole(newUser.getUserRole());
                    user.setUserEmail(newUser.getUserEmail());
                    user.setUserPassword(newUser.getUserPassword());
                    return userRepository.change(user);
                })
                .orElseGet(() -> userRepository.save(newUser));
    }

    @Override
    public User getCurrentUser() {
        // read principal out of security context and set it to session
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        validatePrinciple(authentication.getPrincipal());
        String loggedInUserName = ((UserDetails) authentication.getPrincipal()).getUsername();
        return this.getByEmail(loggedInUserName);
        //return this.getById(3L);
    }

    private void validatePrinciple(Object principal) {
        if (!(principal instanceof UserDetails)) {
            throw new IllegalArgumentException("Principal can not be null!");
        }
    }
}