package com.pabdi.service;

import com.pabdi.entity.*;

import java.util.List;

public interface CommentService {

    List<Comment> getAll();

    Comment getById(Long id);

    List<Comment> getByTicket(Ticket ticket);

    List<Comment> getByUser(User user);

    Comment add(Comment comment);

    Comment update(Comment comment, Long id);
}
