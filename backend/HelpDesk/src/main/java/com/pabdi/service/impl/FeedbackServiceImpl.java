package com.pabdi.service.impl;

import com.pabdi.entity.Feedback;
import com.pabdi.entity.Ticket;
import com.pabdi.entity.User;
import com.pabdi.repository.impl.FeedbackRepository;
import com.pabdi.service.FeedbackService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("feedbackService")
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    public FeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public List<Feedback> getAll() {
        return feedbackRepository.findAll()
                .orElseThrow(() -> new NoResultException("No feedback found"));
    }

    @Override
    public Feedback getById(Long id) {
        return feedbackRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No feedback found with id " + id));
    }

    @Override
    public Feedback getByTicket(Ticket ticket) {
        return feedbackRepository.findByTicket(ticket)
                .orElseThrow(() -> new NoResultException("No feedback found for ticket id " + ticket.getTicketId()));
    }

    @Override
    public List<Feedback> getByUser(User user) {
        return feedbackRepository.findByUser(user)
                .orElseThrow(() -> new NoResultException("No feedback found for user id " + user.getUserId()));
    }

    @Override
    public List<Feedback> getByRate(Byte rate) {
        return feedbackRepository.findByRate(rate)
                .orElseThrow(() -> new NoResultException("No feedback found with rate " + rate));
    }

    @Transactional
    @Override
    public Feedback add(Feedback history) {
        return feedbackRepository.save(history);
    }

    @Transactional
    @Override
    public Feedback update(Feedback newFeedback, Long id) {
        return feedbackRepository.findById(id)
                .map(feedback -> {
                    feedback.setFeedbackTicket(newFeedback.getFeedbackTicket());
                    feedback.setFeedbackDate(newFeedback.getFeedbackDate());
                    feedback.setFeedbackRate(newFeedback.getFeedbackRate());
                    feedback.setFeedbackText(newFeedback.getFeedbackText());
                    feedbackRepository.change(feedback);
                    return feedback;
                })
                .orElseGet(() -> feedbackRepository.save(newFeedback));
    }
}
