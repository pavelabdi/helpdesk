package com.pabdi.service.impl;

import com.pabdi.entity.*;
import com.pabdi.repository.impl.HistoryRepository;
import com.pabdi.service.HistoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("historyService")
public class HistoryServiceImpl implements HistoryService {

    private final HistoryRepository historyRepository;

    public HistoryServiceImpl(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    @Override
    public List<History> getAll() {
        return historyRepository.findAll()
                .orElseThrow(() -> new NoResultException("No history found"));
    }

    @Override
    public History getById(Long id) {
        return historyRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No history found with id " + id));
    }

    @Override
    public List<History> getByTicket(Ticket ticket) {
        return historyRepository.findByTicket(ticket)
                .orElseThrow(() -> new NoResultException("No history found for ticket id " + ticket.getTicketId()));
    }

    @Override
    public List<History> getByUser(User user) {
        return historyRepository.findByUser(user)
                .orElseThrow(() -> new NoResultException("No history found for user id " + user.getUserId()));
    }

    @Transactional
    @Override
    public History add(History history) {
        return historyRepository.save(history);
    }

    @Transactional
    @Override
    public History update(History newHistory, Long id) {
        return historyRepository.findById(id)
                .map(history -> {
                    history.setHistoryTicket(newHistory.getHistoryTicket());
                    history.setHistoryDate(newHistory.getHistoryDate());
                    history.setHistoryAction(newHistory.getHistoryAction());
                    history.setHistoryUser(newHistory.getHistoryUser());
                    history.setHistoryDescription(newHistory.getHistoryDescription());
                    historyRepository.change(history);
                    return history;
                })
                .orElseGet(() -> historyRepository.save(newHistory));
    }
}
