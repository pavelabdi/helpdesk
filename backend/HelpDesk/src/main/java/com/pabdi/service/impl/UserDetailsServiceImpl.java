package com.pabdi.service.impl;

import com.pabdi.entity.User;
import com.pabdi.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    //private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private final UserService userService;

    public UserDetailsServiceImpl(@Qualifier("userService") UserService userService) {
        this.userService = userService;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found.");
        }
        //log.info("loadUserByUsername() : {}", username);
        String userLogin = user.getUserEmail();
        String userPassword = user.getUserPassword();
        String userRole = user.getUserRole().toString();
        return org.springframework.security.core.userdetails.User
                .withUsername(userLogin)
                .password(userPassword)
                .roles(userRole)
                .build();
    }
}
