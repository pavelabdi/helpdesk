package com.pabdi.controller;

import com.pabdi.dto.CommentDto;
import com.pabdi.dto.FeedbackDto;
import com.pabdi.dto.HistoryDto;
import com.pabdi.dto.TicketDto;
import com.pabdi.entity.*;
import com.pabdi.mapper.CommentMapper;
import com.pabdi.mapper.FeedbackMapper;
import com.pabdi.mapper.HistoryMapper;
import com.pabdi.mapper.TicketMapper;
import com.pabdi.service.*;
import com.pabdi.service.impl.MailServiceImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Validated
@RequestMapping("/tickets")
public class TicketController {

    private final TicketService ticketService;

    private final TicketMapper ticketMapper;

    private final HistoryService historyService;

    private final HistoryMapper historyMapper;

    private final CommentService commentService;

    private final CommentMapper commentMapper;

    private final FeedbackService feedbackService;

    private final FeedbackMapper feedbackMapper;

    private final UserService userService;

    private final MailServiceImpl mailService;

    public TicketController(TicketMapper ticketMapper,
                            @Qualifier("ticketService") TicketService ticketService,
                            HistoryMapper historyMapper,
                            @Qualifier("historyService") HistoryService historyService,
                            CommentMapper commentMapper,
                            @Qualifier("commentService") CommentService commentService,
                            FeedbackMapper feedbackMapper,
                            @Qualifier("feedbackService") FeedbackService feedbackService,
                            @Qualifier("userService") UserService userService,
                            MailServiceImpl mailService) {
        this.ticketMapper = ticketMapper;
        this.ticketService = ticketService;
        this.historyMapper = historyMapper;
        this.historyService = historyService;
        this.commentMapper = commentMapper;
        this.commentService = commentService;
        this.feedbackMapper = feedbackMapper;
        this.feedbackService = feedbackService;
        this.userService = userService;
        this.mailService = mailService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<TicketDto> getById(@PathVariable Long id) {
        Ticket ticketEntity = ticketService.getById(id);
        return ResponseEntity.ok(ticketMapper.toDto(ticketEntity));
    }

    //@CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{id}/history")
    public ResponseEntity<List<HistoryDto>> getHistoryById(@PathVariable Long id) {
        Ticket ticketEntity = ticketService.getById(id);
        List<History> historyEntities = historyService.getByTicket(ticketEntity);
        return ResponseEntity.ok(historyEntities.stream()
                .map(historyMapper::toDto)
                .collect(Collectors.toList()));
    }

    //@CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{id}/comments")
    public ResponseEntity<List<CommentDto>> getCommentsById(@PathVariable Long id) {
        Ticket ticketEntity = ticketService.getById(id);
        List<Comment> commentEntities = commentService.getByTicket(ticketEntity);
        return ResponseEntity.ok(commentEntities.stream()
                .map(commentMapper::toDto)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{id}/feedback")
    public ResponseEntity<FeedbackDto> getFeedback(@PathVariable Long id) {
        Ticket ticketEntity = ticketService.getById(id);
        Feedback feedbackEntity = feedbackService.getByTicket(ticketEntity);
        return ResponseEntity.ok(feedbackMapper.toDto(feedbackEntity));
    }

    @GetMapping("/my")
    public ResponseEntity<List<TicketDto>> getByOwner() {
        List<Ticket> ticketEntities = ticketService.getAllForUser();
        return ResponseEntity.ok(ticketEntities.stream()
                .map(ticketMapper::toDto)
                .collect(Collectors.toList()));
    }

    @GetMapping("/all")
    public ResponseEntity<List<TicketDto>> getAll() {
        List<Ticket> userEntities = ticketService.getAll();
        return ResponseEntity.ok(userEntities.stream()
                .map(ticketMapper::toDto)
                .collect(Collectors.toList()));
    }

    @PostMapping("/{action}")
    public ResponseEntity<Ticket> save(@Valid @RequestBody TicketDto ticket, @PathVariable String action) {
        User user = userService.getCurrentUser();
        Action actionEnum = Action.valueOf(action.toUpperCase());
        Ticket newTicket = ticketMapper.toEntity(ticket, user, actionEnum);
        Comment newComment = new Comment();
        History newHistory = new History();

        if (!ticket.getComment().isEmpty()){
            newComment = commentMapper.toEntity(ticket.getComment(), newTicket, user);
        }

        if (actionEnum == Action.CREATE) {
            newHistory = historyMapper.toEntity(newTicket, user, actionEnum);
            //mailService.sendAcceptableMail(newTicket, actionEnum);
        }

        return ResponseEntity.ok(ticketService.add(newTicket, newComment, newHistory));
    }

    @PostMapping("/{id}/feedback")
    public ResponseEntity<Feedback> postFeedback(@PathVariable Long id, @RequestBody FeedbackDto feedback) {
        Ticket ticket = ticketService.getById(id);
        return ResponseEntity.ok(feedbackService.add(feedbackMapper.toEntity(feedback, ticket)));
    }

    @PostMapping("/{id}/comment")
    public ResponseEntity<Comment> postComment(@PathVariable Long id, @RequestBody CommentDto comment) {
        Ticket ticket = ticketService.getById(id);
        User user = userService.getCurrentUser();
        return ResponseEntity.ok(commentService.add(commentMapper.toEntity(comment.getText(), ticket, user)));
    }

//    @PostMapping("/{id}/comment")
//    public ResponseEntity<Comment> postComment(@PathVariable Long id, @RequestBody CommentDto comment) {
//        // read principal out of security context and set it to session
//        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
//        validatePrinciple(authentication.getPrincipal());
//        String loggedInUser = ((UserDetails) authentication.getPrincipal()).getUsername();
//        User user = userService.getByEmail(loggedInUser);
//        Ticket ticket = ticketService.getById(id);
//        return ResponseEntity.ok(commentService.add(commentMapper.toEntity(comment, ticket, user)));
//    }

//    @PutMapping("/{id}")
//    public ResponseEntity<Ticket> change(@RequestBody TicketDto newTicket, @PathVariable Long id) {
//        User user = userService.getCurrentUser();
//        return ResponseEntity.ok(ticketService.update(ticketMapper.toEntity(newTicket, user), id));
//    }

    //@CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/{id}/{action}")
    public ResponseEntity<Ticket> changeState(@PathVariable Long id, @PathVariable String action) {
        Ticket ticket = ticketService.getById(id);
        User user = userService.getCurrentUser();
        Action actionEnum = Action.valueOf(action.toUpperCase());
        State nextState = actionEnum.getState();
        historyService.add(historyMapper.toEntity(ticket, user, actionEnum));
        ticket.setTicketState(nextState);
        if (actionEnum == Action.APPROVE) {
            ticket.setTicketApprover(user);
        }
        if (actionEnum == Action.ASSIGN_TO_ME) {
            ticket.setTicketAssignee(user);
        }
        mailService.sendAcceptableMail(ticket, actionEnum);
        return ResponseEntity.ok(ticketService.update(ticket, id));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    ResponseEntity<Object> handleConstrainViolationException(ConstraintViolationException e){
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", e.getMessage());
        
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

//    private void validatePrinciple(Object principal) {
//        if (!(principal instanceof UserDetails)) {
//            throw new IllegalArgumentException("Principal can not be null!");
//        }
//    }
}
