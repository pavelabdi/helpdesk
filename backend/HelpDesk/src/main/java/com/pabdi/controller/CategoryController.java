package com.pabdi.controller;

import com.pabdi.entity.Category;
import com.pabdi.service.CategoryService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(@Qualifier("categoryService") CategoryService categoryService){
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity<List<String>> getAll() {
        List<Category> categoryEntities = categoryService.getAll();
        return ResponseEntity.ok(categoryEntities.stream()
                .map(Category::getCategoryName)
                .collect(Collectors.toList()));
    }
}
