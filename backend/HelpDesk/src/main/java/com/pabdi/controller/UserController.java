package com.pabdi.controller;

import com.pabdi.dto.UserDto;
import com.pabdi.entity.User;
import com.pabdi.mapper.UserMapper;
import com.pabdi.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    private final UserMapper userMapper;

    public UserController(UserMapper userMapper, @Qualifier("userService") UserService userService){
        this.userMapper = userMapper;
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getById(@PathVariable Long id){
        User userEntity = userService.getById(id);
        return ResponseEntity.ok(userMapper.toDto(userEntity));
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getAll(){
        List<User> userEntities = userService.getAll();
        return ResponseEntity.ok(userEntities.stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList()));
    }

    @PostMapping
    public ResponseEntity<User> save(@RequestBody UserDto user){
        return ResponseEntity.ok(userService.add(userMapper.toEntity(user)));
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> change(@RequestBody UserDto newUser, @PathVariable Long id) {
        return ResponseEntity.ok(userService.update(userMapper.toEntity(newUser), id));
    }
}
