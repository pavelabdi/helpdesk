package com.pabdi.controller;

import com.pabdi.entity.Attachment;
import com.pabdi.service.AttachmentService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import java.net.URI;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/attachments")
public class AttachmentController {

    private AttachmentService attachmentService;

    public AttachmentController(@Qualifier("attachmentService") AttachmentService attachmentService){
        this.attachmentService = attachmentService;
    }

    private String getFileNameHeader(Attachment attachment){
        return "attachment; filename=\"" + attachment.getAttachmentName() + "\"";
    }

//    @GetMapping(value = "/", produces = MediaType.APPLICATION_OCTET_STREAM)
//    public ResponseEntity getAttachmentAll() {
//        Attachment attachment = attachmentService.getById(id);
//        return ResponseEntity.ok()
//                .header(HttpHeaders.CONTENT_DISPOSITION, getFileNameHeader(attachment))
//                .body(attachment.getAttachmentBlob());
//    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_OCTET_STREAM)
    public ResponseEntity getAttachment(@PathVariable Long id) {
        Attachment attachment = attachmentService.getById(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, getFileNameHeader(attachment))
                .body(attachment.getAttachmentBlob());
    }

    @PostMapping
    public ResponseEntity addAttachment(@RequestParam(name = "file") CommonsMultipartFile file){
        Attachment attachment = new Attachment();
        attachment.setAttachmentName(file.getName());
        attachment.setAttachmentBlob(file.getBytes());
        attachmentService.add(attachment);
        URI location = fromCurrentRequest().buildAndExpand(attachment.getAttachmentId()).toUri();
        return ResponseEntity.created(location).build();
    }
}
